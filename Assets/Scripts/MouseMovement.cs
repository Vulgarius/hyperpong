﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMovement : MonoBehaviour
{
    public Transform objectToMove;

    private void Start()
    {
        Cursor.visible = false;
    }

    void Update()
    {
        Vector3 mouse = Input.mousePosition;
        Ray castPoint = Camera.main.ScreenPointToRay(mouse);
        if (Physics.Raycast(castPoint, out var hit, Mathf.Infinity))
        {
            Vector3 impact = hit.point;
            var transform1 = objectToMove.transform;
            transform1.position = new Vector3(impact.x,impact.y,transform1.position.z);
        }
    }
}
