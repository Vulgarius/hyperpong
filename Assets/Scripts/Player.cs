﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.ProBuilder;

public class Player : MonoBehaviour
{
    void OnCollisionEnter(Collision other)
    {
        if (other.rigidbody.velocity.z > 0)
        {
            other.rigidbody.AddForce(0,0,50,ForceMode.Impulse);
        }
        else
        {
            other.rigidbody.AddForce(0,0,-50,ForceMode.Impulse);
        }
    }
}
