﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    void OnCollisionEnter(Collision other)
    {
        other.rigidbody.AddForce(0,0,-50,ForceMode.Impulse);
    }
}
