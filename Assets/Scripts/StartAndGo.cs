﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartAndGo : MonoBehaviour
{

    private Rigidbody _rigidbody;

    public GameObject spherePrefab;

    private int _rand;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WaitAndKickoff(_rand));
    }

    private void Update()
    {
        if (spherePrefab.transform.position.z < -50f || spherePrefab.transform.position.z > 50f)
        {
            Debug.Log(spherePrefab.transform.position);
            Destroy(spherePrefab);
            _rand = UnityEngine.Random.Range(0, 2);
            StartCoroutine(WaitAndKickoff(_rand));
        }
    }

    IEnumerator WaitAndKickoff(int rand)
    {
        spherePrefab = Instantiate(spherePrefab, transform.position, transform.rotation);
        spherePrefab.name = "Ball";
        _rigidbody = spherePrefab.GetComponent<Rigidbody>();
        _rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        yield return new WaitForSeconds(2f);
        _rigidbody.constraints = RigidbodyConstraints.None;
        if (rand == 0)
        {
            _rigidbody.AddForce(0,0,-100,ForceMode.Impulse);
        }
        else
        {
            _rigidbody.AddForce(0,0,100,ForceMode.Impulse);
        }
    }
}
